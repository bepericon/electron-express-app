module.exports = () => {
  // Modulos extras.
  const 
    fs = require('fs'),
    path = require('path');

  //Obtenemos el archivo de configuracion.
  const config = JSON.parse(fs.readFileSync('config.json'));

  //Cargamos el modulo de express.
  var express = require('express'),
      app = express();

  app.use(express.static(__dirname + '/app-express/public'));

  //Configuramos motor de renderizado.
  app.set('views', __dirname + "/views");
  app.set('view engine', 'html');
  app.engine('html', require('ejs').renderFile);

  //CSS
  app.use("/mycss", express.static(__dirname + "/public/css"));
  //Bootstrap.
  // app.use("/js", express.static("../../../node_modules/bootstrap/dist/js"));
  // app.use("/css", express.static("../../../node_modules/bootstrap/dist/css"));

  //Ruteo
  app.get("/", (req, res) => {
    res.render("index");
  });

  app.get("/login", (req,res) => {
    res.render("login");
  });
    
  app.get("/signup", (req,res) => {
    res.render("signup");
  });
    
  //Iniciamos el servidor
  app.listen(config.server.port, () => {
    console.log('listening on port '+ config.server.port +'...');
  });
};