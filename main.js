//Varios modulos desde electron.
//ipcMain es para escuchar los eventos que enviamos desde las ventanas.
const { app, BrowserWindow, Menu,ipcMain} = require("electron");

//Modulos extras.
const 
  url = require("url"),
  path = require("path"),
  fs = require("fs"),
  express = require('express');

//Obtenemos el archivo de configuracion.
const config = JSON.parse(fs.readFileSync('config.json'));

//Reload en tiempo real.
require('electron-reload')(__dirname);

//Variables globales de las ventanas.
let 
  mainWindow,
  mainWindowMenu;

const DevToolsMenu = {
  label: "DevTools",
  submenu: [
    {
      label: "Show/Hide Dev Tools",
      accelerator: "Ctrl+D",
      click(item, focusedWindow){
        focusedWindow.toggleDevTools();
      }
    },
    {
      //Para refrescar la ventana
      role: "reload"
    }
  ]
};

const webPreferences = {
  nodeIntegration: false
};

//Metodo para crear la ventana principal
function createMainWindow(){

  //Instaciacion de Express App
  app.server = require(__dirname + '/app-express/app')();

  //Creamos la ventana.
  mainWindow = new BrowserWindow({
    webPreferences: webPreferences,
    center: true,
    useContentSize: true,
    resizable: false,
    icon: '/app-express/public/images/icon.png'
  });

  // Cargamos el index.html de la app.
  mainWindow.loadURL('http://'+config.server.host+':'+config.server.port+'/');

  //Creamos un menu a partir de un arreglo propio.
  mainWindowMenu = Menu.buildFromTemplate([DevToolsMenu]);
  Menu.setApplicationMenu(mainWindowMenu);

  //Cuando la ventana principal se cierre la aplicacion se cerrara.
  mainWindow.on("closed", () => {
    app.quit();
  });
};

//Cuando la app lance el evento ready, se crea y configura la ventana inicial.
app.on("ready", createMainWindow);

